<?xml version="1.0" encoding="UTF-8"?>

<ValueSet xmlns="http://hl7.org/fhir">
  <id value="data-types"/>
  <meta>
    <lastUpdated value="2015-07-14T16:14:24.220+00:00"/>
    <profile value="http://hl7.org/fhir/StructureDefinition/valueset-shareable-definition"/>
  </meta>
  <text>
    <status value="generated"/>
    <div xmlns="http://www.w3.org/1999/xhtml">
      <h2>DataType</h2>
      <p>The type of an element - one of the FHIR data types</p>
      <p>This value set has an inline code system http://hl7.org/fhir/data-types, which defines the following codes:</p>
      <table class="codes">
        <tr>
          <td>
            <b>Code</b>
          </td>
          <td>
            <b>Display</b>
          </td>
          <td>
            <b>Definition</b>
          </td>
        </tr>
        <tr>
          <td>Address
            <a name="Address"> </a>
          </td>
          <td>Address</td>
          <td>There is a variety of postal address formats defined around the world. This format defines a superset that is the basis for all addresses around the world.</td>
        </tr>
        <tr>
          <td>Age
            <a name="Age"> </a>
          </td>
          <td>Age</td>
          <td>A duration (length of time) with a UCUM code</td>
        </tr>
        <tr>
          <td>Annotation
            <a name="Annotation"> </a>
          </td>
          <td>Annotation</td>
          <td>A  text note which also  contains information about who made the statement and when.</td>
        </tr>
        <tr>
          <td>Attachment
            <a name="Attachment"> </a>
          </td>
          <td>Attachment</td>
          <td>For referring to data content defined in other formats.</td>
        </tr>
        <tr>
          <td>BackboneElement
            <a name="BackboneElement"> </a>
          </td>
          <td>BackboneElement</td>
          <td>Base definition for all elements that are defined inside a resource - but not those in a data type.</td>
        </tr>
        <tr>
          <td>CodeableConcept
            <a name="CodeableConcept"> </a>
          </td>
          <td>CodeableConcept</td>
          <td>A concept that may be defined by a formal reference to a terminology or ontology or may be provided by text.</td>
        </tr>
        <tr>
          <td>Coding
            <a name="Coding"> </a>
          </td>
          <td>Coding</td>
          <td>A reference to a code defined by a terminology system.</td>
        </tr>
        <tr>
          <td>ContactPoint
            <a name="ContactPoint"> </a>
          </td>
          <td>ContactPoint</td>
          <td>Details for All kinds of technology mediated contact points for a person or organization, including telephone, email, etc.</td>
        </tr>
        <tr>
          <td>Count
            <a name="Count"> </a>
          </td>
          <td>Count</td>
          <td>A count of a discrete element (no unit)</td>
        </tr>
        <tr>
          <td>Distance
            <a name="Distance"> </a>
          </td>
          <td>Distance</td>
          <td>A measure of distance</td>
        </tr>
        <tr>
          <td>Duration
            <a name="Duration"> </a>
          </td>
          <td>Duration</td>
          <td>A length of time</td>
        </tr>
        <tr>
          <td>Element
            <a name="Element"> </a>
          </td>
          <td>Element</td>
          <td>Base definition for all elements in a resource.</td>
        </tr>
        <tr>
          <td>ElementDefinition
            <a name="ElementDefinition"> </a>
          </td>
          <td>ElementDefinition</td>
          <td>Captures constraints on each element within the resource, profile, or extension.</td>
        </tr>
        <tr>
          <td>Extension
            <a name="Extension"> </a>
          </td>
          <td>Extension</td>
          <td>Optional Extensions Element - found in all resources.</td>
        </tr>
        <tr>
          <td>HumanName
            <a name="HumanName"> </a>
          </td>
          <td>HumanName</td>
          <td>A human&#39;s name with the ability to identify parts and usage.</td>
        </tr>
        <tr>
          <td>Identifier
            <a name="Identifier"> </a>
          </td>
          <td>Identifier</td>
          <td>A technical identifier - identifies some entity uniquely and unambiguously.</td>
        </tr>
        <tr>
          <td>Meta
            <a name="Meta"> </a>
          </td>
          <td>Meta</td>
          <td>The metadata about a resource. This is content in the resource that is maintained by the infrastructure. Changes to the content may not always be associated with version changes to the resource.</td>
        </tr>
        <tr>
          <td>Money
            <a name="Money"> </a>
          </td>
          <td>Money</td>
          <td>An amount of money. With regard to precision, see [[X]]</td>
        </tr>
        <tr>
          <td>Narrative
            <a name="Narrative"> </a>
          </td>
          <td>Narrative</td>
          <td>A human-readable formatted text, including images.</td>
        </tr>
        <tr>
          <td>Period
            <a name="Period"> </a>
          </td>
          <td>Period</td>
          <td>A time period defined by a start and end date and optionally time.</td>
        </tr>
        <tr>
          <td>Quantity
            <a name="Quantity"> </a>
          </td>
          <td>Quantity</td>
          <td>A measured amount (or an amount that can potentially be measured). Note that measured amounts include amounts that are not precisely quantified, including amounts involving arbitrary units and floating currencies.</td>
        </tr>
        <tr>
          <td>Range
            <a name="Range"> </a>
          </td>
          <td>Range</td>
          <td>A set of ordered Quantities defined by a low and high limit.</td>
        </tr>
        <tr>
          <td>Ratio
            <a name="Ratio"> </a>
          </td>
          <td>Ratio</td>
          <td>A relationship of two Quantity values - expressed as a numerator and a denominator.</td>
        </tr>
        <tr>
          <td>Reference
            <a name="Reference"> </a>
          </td>
          <td>Reference</td>
          <td>A reference from one resource to another.</td>
        </tr>
        <tr>
          <td>SampledData
            <a name="SampledData"> </a>
          </td>
          <td>SampledData</td>
          <td>A series of measurements taken by a device, with upper and lower limits. There may be more than one dimension in the data.</td>
        </tr>
        <tr>
          <td>Signature
            <a name="Signature"> </a>
          </td>
          <td>Signature</td>
          <td>A digital signature along with supporting context. The signature may be electronic/cryptographic in nature, or a graphical image representing a hand-written signature, or a signature process. Different Signature approaches have different utilities.</td>
        </tr>
        <tr>
          <td>Timing
            <a name="Timing"> </a>
          </td>
          <td>Timing</td>
          <td>Specifies an event that may occur multiple times. Timing schedules are used to record when things are expected or requested to occur. The most common usage is in dosage instructions for medications. They are also used when planning care of various kinds.</td>
        </tr>
        <tr>
          <td>base64Binary
            <a name="base64Binary"> </a>
          </td>
          <td>base64Binary</td>
          <td>A stream of bytes</td>
        </tr>
        <tr>
          <td>boolean
            <a name="boolean"> </a>
          </td>
          <td>boolean</td>
          <td>Value of &quot;true&quot; or &quot;false&quot;</td>
        </tr>
        <tr>
          <td>code
            <a name="code"> </a>
          </td>
          <td>code</td>
          <td>A string which has at least one character and no leading or trailing whitespace and where there is no whitespace other than single spaces in the contents</td>
        </tr>
        <tr>
          <td>date
            <a name="date"> </a>
          </td>
          <td>date</td>
          <td>A date, or partial date (e.g. just year or year + month). There is no time zone. The format is a union of the schema types gYear, gYearMonth and date.  Dates SHALL be valid dates.</td>
        </tr>
        <tr>
          <td>dateTime
            <a name="dateTime"> </a>
          </td>
          <td>dateTime</td>
          <td>A date, date-time or partial date (e.g. just year or year + month).  If hours and minutes are specified, a time zone SHALL be populated. The format is a union of the schema types gYear, gYearMonth, date and dateTime. Seconds must be provided due to schema type constraints but may be zero-filled and may be ignored.  Dates SHALL be valid dates.</td>
        </tr>
        <tr>
          <td>decimal
            <a name="decimal"> </a>
          </td>
          <td>decimal</td>
          <td>A rational number with implicit precision</td>
        </tr>
        <tr>
          <td>id
            <a name="id"> </a>
          </td>
          <td>id</td>
          <td>Any combination of lowercase letters, numerals, &quot;-&quot; and &quot;.&quot;, with a length limit of 36 characters.  (This might be an integer, an unprefixed OID, UUID or any other identifier pattern that meets these constraints.)  Systems SHALL send ids as lower-case but SHOULD interpret them case-insensitively.</td>
        </tr>
        <tr>
          <td>instant
            <a name="instant"> </a>
          </td>
          <td>instant</td>
          <td>An instant in time - known at least to the second</td>
        </tr>
        <tr>
          <td>integer
            <a name="integer"> </a>
          </td>
          <td>integer</td>
          <td>A whole number</td>
        </tr>
        <tr>
          <td>oid
            <a name="oid"> </a>
          </td>
          <td>oid</td>
          <td>An oid represented as a URI</td>
        </tr>
        <tr>
          <td>positiveInt
            <a name="positiveInt"> </a>
          </td>
          <td>positiveInt</td>
          <td>An integer with a value that is positive (e.g. &gt;0)</td>
        </tr>
        <tr>
          <td>string
            <a name="string"> </a>
          </td>
          <td>string</td>
          <td>A sequence of Unicode characters</td>
        </tr>
        <tr>
          <td>time
            <a name="time"> </a>
          </td>
          <td>time</td>
          <td>A time during the day, with no date specified</td>
        </tr>
        <tr>
          <td>unsignedInt
            <a name="unsignedInt"> </a>
          </td>
          <td>unsignedInt</td>
          <td>An integer with a value that is not negative (e.g. &gt;= 0)</td>
        </tr>
        <tr>
          <td>uri
            <a name="uri"> </a>
          </td>
          <td>uri</td>
          <td>String of characters used to identify a name or a resource</td>
        </tr>
        <tr>
          <td>uuid
            <a name="uuid"> </a>
          </td>
          <td>uuid</td>
          <td>A UUID, represented as a URI</td>
        </tr>
      </table>
    </div>
  </text>
  <extension url="http://hl7.org/fhir/StructureDefinition/valueset-oid">
    <valueUri value="2.16.840.1.113883.4.642.2.3"/>
  </extension>
  <url value="http://hl7.org/fhir/ValueSet/data-types"/>
  <version value="0.5.0"/>
  <name value="DataType"/>
  <publisher value="HL7 (FHIR Project)"/>
  <contact>
    <telecom>
      <system value="url"/>
      <value value="http://hl7.org/fhir"/>
    </telecom>
    <telecom>
      <system value="email"/>
      <value value="fhir@lists.hl7.org"/>
    </telecom>
  </contact>
  <description value="The type of an element - one of the FHIR data types"/>
  <status value="draft"/>
  <experimental value="false"/>
  <date value="2015-07-14T16:14:24+00:00"/>
  <define>
    <extension url="http://hl7.org/fhir/StructureDefinition/valueset-oid">
      <valueUri value="urn:oid:2.16.840.1.113883.4.642.1.3"/>
    </extension>
    <system value="http://hl7.org/fhir/data-types"/>
    <version value="0.5.0"/>
    <caseSensitive value="true"/>
    <concept>
      <code value="Address"/>
      <display value="Address"/>
      <definition value="There is a variety of postal address formats defined around the world. This format defines a superset that is the basis for all addresses around the world."/>
    </concept>
    <concept>
      <code value="Age"/>
      <display value="Age"/>
      <definition value="A duration (length of time) with a UCUM code"/>
    </concept>
    <concept>
      <code value="Annotation"/>
      <display value="Annotation"/>
      <definition value="A  text note which also  contains information about who made the statement and when."/>
    </concept>
    <concept>
      <code value="Attachment"/>
      <display value="Attachment"/>
      <definition value="For referring to data content defined in other formats."/>
    </concept>
    <concept>
      <code value="BackboneElement"/>
      <display value="BackboneElement"/>
      <definition value="Base definition for all elements that are defined inside a resource - but not those in a data type."/>
    </concept>
    <concept>
      <code value="CodeableConcept"/>
      <display value="CodeableConcept"/>
      <definition value="A concept that may be defined by a formal reference to a terminology or ontology or may be provided by text."/>
    </concept>
    <concept>
      <code value="Coding"/>
      <display value="Coding"/>
      <definition value="A reference to a code defined by a terminology system."/>
    </concept>
    <concept>
      <code value="ContactPoint"/>
      <display value="ContactPoint"/>
      <definition value="Details for All kinds of technology mediated contact points for a person or organization, including telephone, email, etc."/>
    </concept>
    <concept>
      <code value="Count"/>
      <display value="Count"/>
      <definition value="A count of a discrete element (no unit)"/>
    </concept>
    <concept>
      <code value="Distance"/>
      <display value="Distance"/>
      <definition value="A measure of distance"/>
    </concept>
    <concept>
      <code value="Duration"/>
      <display value="Duration"/>
      <definition value="A length of time"/>
    </concept>
    <concept>
      <code value="Element"/>
      <display value="Element"/>
      <definition value="Base definition for all elements in a resource."/>
    </concept>
    <concept>
      <code value="ElementDefinition"/>
      <display value="ElementDefinition"/>
      <definition value="Captures constraints on each element within the resource, profile, or extension."/>
    </concept>
    <concept>
      <code value="Extension"/>
      <display value="Extension"/>
      <definition value="Optional Extensions Element - found in all resources."/>
    </concept>
    <concept>
      <code value="HumanName"/>
      <display value="HumanName"/>
      <definition value="A human&#39;s name with the ability to identify parts and usage."/>
    </concept>
    <concept>
      <code value="Identifier"/>
      <display value="Identifier"/>
      <definition value="A technical identifier - identifies some entity uniquely and unambiguously."/>
    </concept>
    <concept>
      <code value="Meta"/>
      <display value="Meta"/>
      <definition value="The metadata about a resource. This is content in the resource that is maintained by the infrastructure. Changes to the content may not always be associated with version changes to the resource."/>
    </concept>
    <concept>
      <code value="Money"/>
      <display value="Money"/>
      <definition value="An amount of money. With regard to precision, see [[X]]"/>
    </concept>
    <concept>
      <code value="Narrative"/>
      <display value="Narrative"/>
      <definition value="A human-readable formatted text, including images."/>
    </concept>
    <concept>
      <code value="Period"/>
      <display value="Period"/>
      <definition value="A time period defined by a start and end date and optionally time."/>
    </concept>
    <concept>
      <code value="Quantity"/>
      <display value="Quantity"/>
      <definition value="A measured amount (or an amount that can potentially be measured). Note that measured amounts include amounts that are not precisely quantified, including amounts involving arbitrary units and floating currencies."/>
    </concept>
    <concept>
      <code value="Range"/>
      <display value="Range"/>
      <definition value="A set of ordered Quantities defined by a low and high limit."/>
    </concept>
    <concept>
      <code value="Ratio"/>
      <display value="Ratio"/>
      <definition value="A relationship of two Quantity values - expressed as a numerator and a denominator."/>
    </concept>
    <concept>
      <code value="Reference"/>
      <display value="Reference"/>
      <definition value="A reference from one resource to another."/>
    </concept>
    <concept>
      <code value="SampledData"/>
      <display value="SampledData"/>
      <definition value="A series of measurements taken by a device, with upper and lower limits. There may be more than one dimension in the data."/>
    </concept>
    <concept>
      <code value="Signature"/>
      <display value="Signature"/>
      <definition value="A digital signature along with supporting context. The signature may be electronic/cryptographic in nature, or a graphical image representing a hand-written signature, or a signature process. Different Signature approaches have different utilities."/>
    </concept>
    <concept>
      <code value="Timing"/>
      <display value="Timing"/>
      <definition value="Specifies an event that may occur multiple times. Timing schedules are used to record when things are expected or requested to occur. The most common usage is in dosage instructions for medications. They are also used when planning care of various kinds."/>
    </concept>
    <concept>
      <code value="base64Binary"/>
      <display value="base64Binary"/>
      <definition value="A stream of bytes"/>
    </concept>
    <concept>
      <code value="boolean"/>
      <display value="boolean"/>
      <definition value="Value of &quot;true&quot; or &quot;false&quot;"/>
    </concept>
    <concept>
      <code value="code"/>
      <display value="code"/>
      <definition value="A string which has at least one character and no leading or trailing whitespace and where there is no whitespace other than single spaces in the contents"/>
    </concept>
    <concept>
      <code value="date"/>
      <display value="date"/>
      <definition value="A date, or partial date (e.g. just year or year + month). There is no time zone. The format is a union of the schema types gYear, gYearMonth and date.  Dates SHALL be valid dates."/>
    </concept>
    <concept>
      <code value="dateTime"/>
      <display value="dateTime"/>
      <definition value="A date, date-time or partial date (e.g. just year or year + month).  If hours and minutes are specified, a time zone SHALL be populated. The format is a union of the schema types gYear, gYearMonth, date and dateTime. Seconds must be provided due to schema type constraints but may be zero-filled and may be ignored.  Dates SHALL be valid dates."/>
    </concept>
    <concept>
      <code value="decimal"/>
      <display value="decimal"/>
      <definition value="A rational number with implicit precision"/>
    </concept>
    <concept>
      <code value="id"/>
      <display value="id"/>
      <definition value="Any combination of lowercase letters, numerals, &quot;-&quot; and &quot;.&quot;, with a length limit of 36 characters.  (This might be an integer, an unprefixed OID, UUID or any other identifier pattern that meets these constraints.)  Systems SHALL send ids as lower-case but SHOULD interpret them case-insensitively."/>
    </concept>
    <concept>
      <code value="instant"/>
      <display value="instant"/>
      <definition value="An instant in time - known at least to the second"/>
    </concept>
    <concept>
      <code value="integer"/>
      <display value="integer"/>
      <definition value="A whole number"/>
    </concept>
    <concept>
      <code value="oid"/>
      <display value="oid"/>
      <definition value="An oid represented as a URI"/>
    </concept>
    <concept>
      <code value="positiveInt"/>
      <display value="positiveInt"/>
      <definition value="An integer with a value that is positive (e.g. &gt;0)"/>
    </concept>
    <concept>
      <code value="string"/>
      <display value="string"/>
      <definition value="A sequence of Unicode characters"/>
    </concept>
    <concept>
      <code value="time"/>
      <display value="time"/>
      <definition value="A time during the day, with no date specified"/>
    </concept>
    <concept>
      <code value="unsignedInt"/>
      <display value="unsignedInt"/>
      <definition value="An integer with a value that is not negative (e.g. &gt;= 0)"/>
    </concept>
    <concept>
      <code value="uri"/>
      <display value="uri"/>
      <definition value="String of characters used to identify a name or a resource"/>
    </concept>
    <concept>
      <code value="uuid"/>
      <display value="uuid"/>
      <definition value="A UUID, represented as a URI"/>
    </concept>
  </define>
</ValueSet>