{
  "resourceType": "Conformance",
  "id": "conformance-uslabreport-receiver",
  "text": {
    "status": "generated",
    "div": "<div>\n\t\t\t<h2>USLabReport Receiver</h2>\n\t\t\t<p>(Requirements Definition)</p>\n\t\t\t<p>Published: 2014-12-02 (draft)</p>\n\t\t\t<p>Published by: <b>\n\t\t\t\t\t<a href=\"http://www.hl7.org/\">Published by: HL7 Orders and Observation Workgroup</a>\n\t\t\t\t</b> Primary Author: Eric Haas  Health eData Inc</p>\n\t\t\t<p>This profile defines the expected capabilities of the USLabReport Receiver actor when conforming to the <a href=\"uslabreport.html\">The US Receiver Report Implementation (USLabReport)</a>.  This actor is the receiver of a laboratory test report and declares conformance to RESTful FHIR and FHIR profiles defined in this guide.  The order reference one or more FHIR resources conforming to profiles outlined in the USLabReport guide.</p>\n\t\t\t<h2>General</h2>\n\t\t\t<table>\n\t\t\t\t<tbody>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>FHIR Version:</th>\n\t\t\t\t\t\t<td>0.8</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>Supported formats:</th>\n\t\t\t\t\t\t<td>xml, json</td>\n\t\t\t\t\t</tr>\n\t\t\t\t</tbody>\n\t\t\t</table>\n\t\t\t<h2>REST  behavior</h2>\n\t\t\t<p>\n\t\t\t\t<b>Mode:</b> Server\n\t\t\t</p>\n\t\t\t<p>This conformance resource assumes the USLabReport Receiver is the server, in other words, operating in 'Pull'  or 'Push/Pull' RESTful interface.  The USLabReport Receiver MUST support querying one or more resources outlined by the <a href=\"uslabreport-guide.html\">USLabReport Guide</a>. The USLabReport Receiver MUST use all the vocabularies and value set constraints defined by the individual resource profiles used by USLabReport. The USLabReport Receiver MUST implement REST behavior according to the <a href=\"http.html\">FHIR specification</a> and MUST be able to handle errors gracefully from Query Responders who may not support the submitted query.</p>\n\t\t\t<p>\n\t\t\t\t<b>Security:</b>\n\t\t\t</p>\n\t\t\t<p>Implementations must meet the security requirements documented in the <a href=\"uslabreport-guide.html#assumptions\">USLabReport Guide assumptions</a>.</p>\n\t\t\t<h3>Summary</h3>\n\t\t\t<table class=\"grid\">\n\t\t\t\t<thead>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>Resource</th>\n\t\t\t\t\t\t<th>Search</th>\n\t\t\t\t\t\t<th>Read</th>\n\t\t\t\t\t\t<th>Read Version</th>\n\t\t\t\t\t\t<th>Instance History</th>\n\t\t\t\t\t\t<th>Resource History</th>\n\t\t\t\t\t\t<th>Validate</th>\n\t\t\t\t\t\t<th>Create</th>\n\t\t\t\t\t\t<th>Update</th>\n\t\t\t\t\t\t<th>Delete</th>\n\t\t\t\t\t</tr>\n\t\t\t\t</thead>\n\t\t\t\t<tbody>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>DiagnosticReport</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<a title=\"Allows a user to search for existing DiagnosticReport\" href=\"#DiagnosticReport-search-type\">Yes</a>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<a title=\"Allows retrieval of a specific known DiagnosticReport\" href=\"#DiagnosticReport-read\">Yes</a>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<a title=\"Allows retrieval of a specific version of a DiagnosticReport\" href=\"#DiagnosticReport-vread\">Yes</a>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<a title=\"Allows review of changes to a DiagnosticReport over time\" href=\"#DiagnosticReport-history-instance\">Yes</a>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td/>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<a title=\"Allows a client to verify whether a particular new DiagnosticReport or revision of an existing DiagnosticReport would be accepted based on validation and other business rules.  Useful for some workflows\" href=\"#DiagnosticReport-validate\">Yes</a>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<a title=\"Allows defining a new DiagnosticReport\" href=\"#DiagnosticReport-create\">Yes</a>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<a title=\"Allows editing of an existing DiagnosticReport.  Servers may choose to prohibit certain types of edits, instead requiring the creation of a new DiagnosticReport (and potentially the retiring of the existing DiagnosticReport).  Servers may also limit who can change particular DiagnosticReport.\" href=\"#DiagnosticReport-update\">Yes</a>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td/>\n\t\t\t\t\t</tr>\n\t\t\t\t</tbody>\n\t\t\t</table>\n\t\t\t<br/>\n\t\t\t<br/>\n\t\t\t<h3>\n\t\t\t\t<a href=\"diagnosticreport.html\">DiagnosticReport</a>\n\t\t\t</h3>\n\t\t\t<h4>Interactions</h4>\n\t\t\t<table class=\"list\">\n\t\t\t\t<thead>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>Name</th>\n\t\t\t\t\t\t<th>Description</th>\n\t\t\t\t\t</tr>\n\t\t\t\t</thead>\n\t\t\t\t<tbody>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>\n\t\t\t\t\t\t\t<a name=\"DiagnosticReport-search-type\">&nbsp;</a>\n\t\t\t\t\t\t\t<span>search-type</span>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<p>Allows a user to search for existing DiagnosticReport</p>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>\n\t\t\t\t\t\t\t<a name=\"DiagnosticReport-read\">&nbsp;</a>\n\t\t\t\t\t\t\t<span>read</span>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<p>Allows retrieval of a specific known DiagnosticReport</p>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>\n\t\t\t\t\t\t\t<a name=\"DiagnosticReport-vread\">&nbsp;</a>\n\t\t\t\t\t\t\t<span>vread</span>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<p>Allows retrieval of a specific version of a DiagnosticReport</p>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>\n\t\t\t\t\t\t\t<a name=\"DiagnosticReport-history-instance\">&nbsp;</a>\n\t\t\t\t\t\t\t<span>history-instance</span>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<p>Allows review of changes to a DiagnosticReport over time</p>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>\n\t\t\t\t\t\t\t<a name=\"DiagnosticReport-create\">&nbsp;</a>\n\t\t\t\t\t\t\t<span>create</span>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<p>Allows defining a new DiagnosticReport</p>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>\n\t\t\t\t\t\t\t<a name=\"DiagnosticReport-update\">&nbsp;</a>\n\t\t\t\t\t\t\t<span>update</span>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<p>Allows editing of an existing DiagnosticReport.  Servers may choose to prohibit certain types of edits, instead requiring the creation of a new DiagnosticReport (and potentially the retiring of the existing DiagnosticReport).  Servers may also limit who can change particular DiagnosticReport.</p>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>\n\t\t\t\t\t\t\t<a name=\"DiagnosticReport-validate\">&nbsp;</a>\n\t\t\t\t\t\t\t<span>validate</span>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<p>Allows a client to verify whether a particular new DiagnosticReport or revision of an existing DiagnosticReport would be accepted based on validation and other business rules.  Useful for some workflows</p>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t</tbody>\n\t\t\t</table>\n\t\t\t<h4>Search</h4>\n\t\t\t<p>Supported Includes: DiagnosticReport.subject, DiagnosticReport.performer, DiagnosticReport.requestDetail, DiagnosticReport.specimen, DiagnosticReport.report</p>\n\t\t\t<h2>REST  behavior</h2>\n\t\t\t<p>\n\t\t\t\t<b>Mode:</b> Server\n\t\t\t</p>\n\t\t\t<p>The following conformance rules assumes the USLabReport Receiver is the client, in other words, operating in 'Push'  RESTful interface.  The USLabReport Receiver MUST support querying one or more resources outlined by the <a href=\"uslabreport.html\">USLabReport Guide</a>. The USLabReport Receiver MUST use all the vocabularies and value set constraints defined by the individual resource profiles used by USLabReport. The USLabReport Receiver MUST implement REST behavior according to the <a href=\"http.html\">FHIR specification</a> and MUST be able to handle errors gracefully from Query Responders who may not support the submitted query.</p>\n\t\t\t<p>\n\t\t\t\t<b>Security:</b>\n\t\t\t</p>\n\t\t\t<p>Implementations must meet the security requirements documented in the <a href=\"uslabreport.html\">USLabReport Guide assumptions</a>.</p>\n\t\t\t<h3>Summary</h3>\n\t\t\t<table class=\"grid\">\n\t\t\t\t<thead>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>Resource</th>\n\t\t\t\t\t\t<th>Search</th>\n\t\t\t\t\t\t<th>Read</th>\n\t\t\t\t\t\t<th>Read Version</th>\n\t\t\t\t\t\t<th>Instance History</th>\n\t\t\t\t\t\t<th>Resource History</th>\n\t\t\t\t\t\t<th>Validate</th>\n\t\t\t\t\t\t<th>Create</th>\n\t\t\t\t\t\t<th>Update</th>\n\t\t\t\t\t\t<th>Delete</th>\n\t\t\t\t\t</tr>\n\t\t\t\t</thead>\n\t\t\t\t<tbody>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>DiagnosticReport</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<a title=\"Allows a user to search for existing DiagnosticReport\" href=\"#DiagnosticReport-search-type\">Yes</a>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<a title=\"Allows retrieval of a specific known DiagnosticReport\" href=\"#DiagnosticReport-read\">Yes</a>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<a title=\"Allows retrieval of a specific version of a DiagnosticReport\" href=\"#DiagnosticReport-vread\">Yes</a>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<a title=\"Allows review of changes to a DiagnosticReport over time\" href=\"#DiagnosticReport-history-instance\">Yes</a>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td/>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<a title=\"Allows a client to verify whether a particular new DiagnosticReport or revision of an existing DiagnosticReport would be accepted based on validation and other business rules.  Useful for some workflows\" href=\"#DiagnosticReport-validate\">Yes</a>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<a title=\"Allows defining a new DiagnosticReport\" href=\"#DiagnosticReport-create\">Yes</a>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<a title=\"Allows editing of an existing DiagnosticReport.  Servers may choose to prohibit certain types of edits, instead requiring the creation of a new DiagnosticReport (and potentially the retiring of the existing DiagnosticReport).  Servers may also limit who can change particular DiagnosticReport.\" href=\"#DiagnosticReport-update\">Yes</a>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t\t<td/>\n\t\t\t\t\t</tr>\n\t\t\t\t</tbody>\n\t\t\t</table>\n\t\t\t<br/>\n\t\t\t<br/>\n\t\t\t<h3>\n\t\t\t\t<a href=\"diagnosticreport.html\">DiagnosticReport</a>\n\t\t\t</h3>\n\t\t\t<h4>Interactions</h4>\n\t\t\t<table class=\"list\">\n\t\t\t\t<thead>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>Name</th>\n\t\t\t\t\t\t<th>Description</th>\n\t\t\t\t\t</tr>\n\t\t\t\t</thead>\n\t\t\t\t<tbody>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>\n\t\t\t\t\t\t\t<a name=\"DiagnosticReport-search-type\">&nbsp;</a>\n\t\t\t\t\t\t\t<span>search-type</span>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<p>Allows a user to search for existing DiagnosticReport</p>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>\n\t\t\t\t\t\t\t<a name=\"DiagnosticReport-read\">&nbsp;</a>\n\t\t\t\t\t\t\t<span>read</span>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<p>Allows retrieval of a specific known DiagnosticReport</p>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>\n\t\t\t\t\t\t\t<a name=\"DiagnosticReport-vread\">&nbsp;</a>\n\t\t\t\t\t\t\t<span>vread</span>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<p>Allows retrieval of a specific version of a DiagnosticReport</p>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>\n\t\t\t\t\t\t\t<a name=\"DiagnosticReport-history-instance\">&nbsp;</a>\n\t\t\t\t\t\t\t<span>history-instance</span>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<p>Allows review of changes to a DiagnosticReport over time</p>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>\n\t\t\t\t\t\t\t<a name=\"DiagnosticReport-create\">&nbsp;</a>\n\t\t\t\t\t\t\t<span>create</span>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<p>Allows defining a new DiagnosticReport</p>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>\n\t\t\t\t\t\t\t<a name=\"DiagnosticReport-update\">&nbsp;</a>\n\t\t\t\t\t\t\t<span>update</span>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<p>Allows editing of an existing DiagnosticReport.  Servers may choose to prohibit certain types of edits, instead requiring the creation of a new DiagnosticReport (and potentially the retiring of the existing DiagnosticReport).  Servers may also limit who can change particular DiagnosticReport.</p>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th>\n\t\t\t\t\t\t\t<a name=\"DiagnosticReport-validate\">&nbsp;</a>\n\t\t\t\t\t\t\t<span>validate</span>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t<p>Allows a client to verify whether a particular new DiagnosticReport or revision of an existing DiagnosticReport would be accepted based on validation and other business rules.  Useful for some workflows</p>\n\t\t\t\t\t\t</td>\n\t\t\t\t\t</tr>\n\t\t\t\t</tbody>\n\t\t\t</table>\n\t\t\t<h4>Search</h4>\n\t\t\t<p>Supported Includes: DiagnosticReport.subject, DiagnosticReport.performer, DiagnosticReport.requestDetail, DiagnosticReport.specimen, DiagnosticReport.report</p>\n\t\t</div>"
  },
  "name": "USLabReport Receiver",
  "publisher": "Published by: HL7 Orders and Observation Workgroup. Primary Author: Eric Haas  Health eData Inc",
  "contact": [
    {
      "telecom": [
        {
          "system": "url",
          "value": "http://www.hl7.org/"
        }
      ]
    }
  ],
  "description": "This profile defines the expected capabilities of the USLabReport Receiver actor when conforming to the [[uslabreport.html|The US Receiver Report Implementation (USLabReport)]].  This actor is the receiver of a laboratory test report and declares conformance to RESTful FHIR and FHIR profiles defined in this guide.  The order reference one or more FHIR resources conforming to profiles outlined in the USLabReport guide.",
  "status": "draft",
  "date": "2014-12-02",
  "fhirVersion": "0.8",
  "acceptUnknown": false,
  "format": [
    "xml",
    "json"
  ],
  "rest": [
    {
      "mode": "server",
      "documentation": "This conformance resource assumes the USLabReport Receiver is the server, in other words, operating in 'Pull'  or 'Push/Pull' RESTful interface.  The USLabReport Receiver MUST support querying one or more resources outlined by the [[uslabreport|USLabReport Guide]]. The USLabReport Receiver MUST use all the vocabularies and value set constraints defined by the individual resource profiles used by USLabReport. The USLabReport Receiver MUST implement REST behavior according to the [[http|FHIR specification]] and MUST be able to handle errors gracefully from Query Responders who may not support the submitted query.",
      "security": {
        "description": "Implementations must meet the security requirements documented in the [[uslabreport|USLabReport Guide assumptions]]."
      },
      "resource": [
        {
          "type": "DiagnosticReport",
          "profile": {
            "reference": "dr-uslab"
          },
          "interaction": [
            {
              "code": "search-type",
              "documentation": "Allows a user to search for existing DiagnosticReport"
            },
            {
              "code": "read",
              "documentation": "Allows retrieval of a specific known DiagnosticReport"
            },
            {
              "code": "vread",
              "documentation": "Allows retrieval of a specific version of a DiagnosticReport"
            },
            {
              "code": "history-instance",
              "documentation": "Allows review of changes to a DiagnosticReport over time"
            },
            {
              "code": "create",
              "documentation": "Allows defining a new DiagnosticReport"
            },
            {
              "code": "update",
              "documentation": "Allows editing of an existing DiagnosticReport.  Servers may choose to prohibit certain types of edits, instead requiring the creation of a new DiagnosticReport (and potentially the retiring of the existing DiagnosticReport).  Servers may also limit who can change particular DiagnosticReport."
            },
            {
              "code": "validate",
              "documentation": "Allows a client to verify whether a particular new DiagnosticReport or revision of an existing DiagnosticReport would be accepted based on validation and other business rules.  Useful for some workflows"
            }
          ],
          "searchInclude": [
            "DiagnosticReport.subject, DiagnosticReport.performer, DiagnosticReport.requestDetail, DiagnosticReport.specimen, DiagnosticReport.report"
          ]
        }
      ]
    },
    {
      "mode": "client",
      "documentation": "The following conformance rules assumes the USLabReport Receiver is the client, in other words, operating in 'Push'  RESTful interface.  The USLabReport Receiver MUST support querying one or more resources outlined by the [[uslabreport|USLabReport Guide]]. The USLabReport Receiver MUST use all the vocabularies and value set constraints defined by the individual resource profiles used by USLabReport. The USLabReport Receiver MUST implement REST behavior according to the [[http|FHIR specification]] and MUST be able to handle errors gracefully from Query Responders who may not support the submitted query.",
      "security": {
        "description": "Implementations must meet the security requirements documented in the [[uslabreport|USLabReport Guide assumptions]]."
      },
      "resource": [
        {
          "type": "DiagnosticReport",
          "profile": {
            "reference": "do-uslab"
          },
          "interaction": [
            {
              "code": "search-type",
              "documentation": "Allows a user to search for existing DiagnosticReport"
            },
            {
              "code": "read",
              "documentation": "Allows retrieval of a specific known DiagnosticReport"
            },
            {
              "code": "vread",
              "documentation": "Allows retrieval of a specific version of a DiagnosticReport"
            },
            {
              "code": "history-instance",
              "documentation": "Allows review of changes to a DiagnosticReport over time"
            },
            {
              "code": "create",
              "documentation": "Allows defining a new DiagnosticReport"
            },
            {
              "code": "update",
              "documentation": "Allows editing of an existing DiagnosticReport.  Servers may choose to prohibit certain types of edits, instead requiring the creation of a new DiagnosticReport (and potentially the retiring of the existing DiagnosticReport).  Servers may also limit who can change particular DiagnosticReport."
            },
            {
              "code": "validate",
              "documentation": "Allows a client to verify whether a particular new DiagnosticReport or revision of an existing DiagnosticReport would be accepted based on validation and other business rules.  Useful for some workflows"
            }
          ],
          "searchInclude": [
            "DiagnosticReport.subject, DiagnosticReport.performer, DiagnosticReport.requestDetail, DiagnosticReport.specimen, DiagnosticReport.report"
          ]
        }
      ]
    }
  ]
}