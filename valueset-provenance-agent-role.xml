<?xml version="1.0" encoding="UTF-8"?>

<ValueSet xmlns="http://hl7.org/fhir">
  <id value="provenance-agent-role"/>
  <meta>
    <lastUpdated value="2015-07-14T16:14:24.220+00:00"/>
    <profile value="http://hl7.org/fhir/StructureDefinition/valueset-shareable-definition"/>
  </meta>
  <text>
    <status value="extensions"/>
    <div xmlns="http://www.w3.org/1999/xhtml">
      <h2>ProvenanceParticipantRole</h2>
      <p>The role that a provenance participant played</p>
      <p>This value set has an inline code system http://hl7.org/fhir/provenance-participant-role, which defines the following codes:</p>
      <table class="codes">
        <tr>
          <td>
            <b>Lvl</b>
          </td>
          <td>
            <b>Code</b>
          </td>
          <td>
            <b>Display</b>
          </td>
          <td>
            <b>Definition</b>
          </td>
          <td>
            <b>Comments</b>
          </td>
        </tr>
        <tr>
          <td>1</td>
          <td>enterer
            <a name="enterer"> </a>
          </td>
          <td>Enterer</td>
          <td>A person entering the data into the originating system</td>
          <td>The data entry person is collected optionally for internal quality control purposes. This includes the transcriptionist for dictated text</td>
        </tr>
        <tr>
          <td>1</td>
          <td>performer
            <a name="performer"> </a>
          </td>
          <td>Performer</td>
          <td>A person, animal, organization or device that who actually and principally carries out the activity</td>
          <td>Device should only be assigned as a performer in circumstances where the device is performing independent of human intervention. Need not be the principal responsible actor</td>
        </tr>
        <tr>
          <td>1</td>
          <td>author
            <a name="author"> </a>
          </td>
          <td>Author</td>
          <td>A party that originates the resource and therefore has responsibility for the information given in the resource and ownership of this resource</td>
          <td>Every resource should have an author</td>
        </tr>
        <tr>
          <td>1</td>
          <td>verifier
            <a name="verifier"> </a>
          </td>
          <td>Verifier</td>
          <td>A person who verifies the correctness and appropriateness of activity</td>
          <td>This is often used in association with the institution that hosts an activity. Verification and Authentication are synonymous</td>
        </tr>
        <tr>
          <td>2</td>
          <td>  legal
            <a name="legal"> </a>
          </td>
          <td>Legal Authenticator</td>
          <td>The person authenticated the content and accepted legal responsibility for its content</td>
          <td/>
        </tr>
        <tr>
          <td>1</td>
          <td>attester
            <a name="attester"> </a>
          </td>
          <td>Attester</td>
          <td>A verifier who attests to the accuracy of the resource</td>
          <td/>
        </tr>
        <tr>
          <td>1</td>
          <td>informant
            <a name="informant"> </a>
          </td>
          <td>Informant</td>
          <td>A person who reported information that contributed to the resource</td>
          <td>e.g., a next of kin who answers questions about the patient&#39;s history</td>
        </tr>
      </table>
    </div>
  </text>
  <extension url="http://hl7.org/fhir/StructureDefinition/valueset-oid">
    <valueUri value="2.16.840.1.113883.4.642.2.269"/>
  </extension>
  <url value="http://hl7.org/fhir/ValueSet/provenance-agent-role"/>
  <version value="0.5.0"/>
  <name value="ProvenanceParticipantRole"/>
  <publisher value="HL7 (FHIR Project)"/>
  <contact>
    <telecom>
      <system value="url"/>
      <value value="http://hl7.org/fhir"/>
    </telecom>
  </contact>
  <contact>
    <telecom>
      <system value="email"/>
      <value value="fhir@lists.hl7.org"/>
    </telecom>
  </contact>
  <description value="The role that a provenance participant played"/>
  <status value="draft"/>
  <experimental value="true"/>
  <extensible value="true"/>
  <date value="2023-05-12"/>
  <define>
    <extension url="http://hl7.org/fhir/StructureDefinition/valueset-oid">
      <valueUri value="urn:oid:2.16.840.1.113883.4.642.1.269"/>
    </extension>
    <system value="http://hl7.org/fhir/provenance-participant-role"/>
    <caseSensitive value="true"/>
    <concept>
      <extension url="http://hl7.org/fhir/StructureDefinition/valueset-comments">
        <valueString value="The data entry person is collected optionally for internal quality control purposes. This includes the transcriptionist for dictated text"/>
      </extension>
      <code value="enterer"/>
      <display value="Enterer"/>
      <definition value="A person entering the data into the originating system"/>
    </concept>
    <concept>
      <extension url="http://hl7.org/fhir/StructureDefinition/valueset-comments">
        <valueString value="Device should only be assigned as a performer in circumstances where the device is performing independent of human intervention. Need not be the principal responsible actor"/>
      </extension>
      <code value="performer"/>
      <display value="Performer"/>
      <definition value="A person, animal, organization or device that who actually and principally carries out the activity"/>
    </concept>
    <concept>
      <extension url="http://hl7.org/fhir/StructureDefinition/valueset-comments">
        <valueString value="Every resource should have an author"/>
      </extension>
      <code value="author"/>
      <display value="Author"/>
      <definition value="A party that originates the resource and therefore has responsibility for the information given in the resource and ownership of this resource"/>
    </concept>
    <concept>
      <extension url="http://hl7.org/fhir/StructureDefinition/valueset-comments">
        <valueString value="This is often used in association with the institution that hosts an activity. Verification and Authentication are synonymous"/>
      </extension>
      <code value="verifier"/>
      <display value="Verifier"/>
      <definition value="A person who verifies the correctness and appropriateness of activity"/>
      <concept>
        <code value="legal"/>
        <display value="Legal Authenticator"/>
        <definition value="The person authenticated the content and accepted legal responsibility for its content"/>
      </concept>
    </concept>
    <concept>
      <code value="attester"/>
      <display value="Attester"/>
      <definition value="A verifier who attests to the accuracy of the resource"/>
    </concept>
    <concept>
      <extension url="http://hl7.org/fhir/StructureDefinition/valueset-comments">
        <valueString value="e.g., a next of kin who answers questions about the patient&#39;s history"/>
      </extension>
      <code value="informant"/>
      <display value="Informant"/>
      <definition value="A person who reported information that contributed to the resource"/>
    </concept>
  </define>
</ValueSet>