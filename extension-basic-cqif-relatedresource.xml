<?xml version="1.0" encoding="UTF-8"?>

<StructureDefinition xmlns="http://hl7.org/fhir">
  <id value="basic-cqif-relatedResource"/>
  <url value="http://hl7.org/fhir/StructureDefinition/basic-cqif-relatedResource"/>
  <name value="relatedResource"/>
  <display value="relatedResource"/>
  <publisher value="Health Level Seven, Inc. - CDS WG"/>
  <contact>
    <telecom>
      <system value="url"/>
      <value value="http://hl7.org/special/committees/dss"/>
    </telecom>
  </contact>
  <description value="Related resources such as additional documentation, supporting evidence, or bibliographic references."/>
  <status value="draft"/>
  <date value="2015-05-30"/>
  <kind value="datatype"/>
  <constrainedType value="Extension"/>
  <abstract value="false"/>
  <contextType value="resource"/>
  <context value="Basic"/>
  <base value="http://hl7.org/fhir/StructureDefinition/Extension"/>
  <snapshot>
    <element>
      <path value="Extension"/>
      <short value="Extension"/>
      <definition value="Related resources such as additional documentation, supporting evidence, or bibliographic references."/>
      <min value="0"/>
      <max value="*"/>
      <type>
        <code value="Extension"/>
      </type>
    </element>
    <element>
      <path value="Extension.id"/>
      <representation value="xmlAttr"/>
      <short value="xml:id (or equivalent in JSON)"/>
      <definition value="unique id for the element within a resource (for internal references)."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="id"/>
      </type>
      <mapping>
        <identity value="rim"/>
        <map value="n/a"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension"/>
      <slicing>
        <discriminator value="url"/>
        <ordered value="true"/>
        <rules value="openAtEnd"/>
      </slicing>
      <short value="Additional Content defined by implementations"/>
      <definition value="May be used to represent additional information that is not part of the basic definition of the element. In order to make the use of extensions safe and manageable, there is a strict set of governance  applied to the definition and use of extensions. Though any implementer is allowed to define an extension, there is a set of requirements that SHALL be met as part of the definition of the extension."/>
      <comments value="There can be no stigma associated with the use of extensions by any application, project, or standard - regardless of the institution or jurisdiction that uses or defines the extensions.  The use of extensions is what allows the FHIR specification to retain a core level of simplicity for everyone."/>
      <alias value="extensions"/>
      <alias value="user content"/>
      <min value="0"/>
      <max value="*"/>
      <type>
        <code value="Extension"/>
      </type>
      <mapping>
        <identity value="rim"/>
        <map value="n/a"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension"/>
      <name value="type"/>
      <short value="Extension"/>
      <definition value="The type of related resource."/>
      <min value="1"/>
      <max value="1"/>
      <type>
        <code value="Extension"/>
      </type>
    </element>
    <element>
      <path value="Extension.extension.id"/>
      <representation value="xmlAttr"/>
      <short value="xml:id (or equivalent in JSON)"/>
      <definition value="unique id for the element within a resource (for internal references)."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="id"/>
      </type>
      <mapping>
        <identity value="rim"/>
        <map value="n/a"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension.extension"/>
      <short value="Additional Content defined by implementations"/>
      <definition value="May be used to represent additional information that is not part of the basic definition of the element. In order to make the use of extensions safe and manageable, there is a strict set of governance  applied to the definition and use of extensions. Though any implementer is allowed to define an extension, there is a set of requirements that SHALL be met as part of the definition of the extension."/>
      <comments value="There can be no stigma associated with the use of extensions by any application, project, or standard - regardless of the institution or jurisdiction that uses or defines the extensions.  The use of extensions is what allows the FHIR specification to retain a core level of simplicity for everyone."/>
      <alias value="extensions"/>
      <alias value="user content"/>
      <min value="0"/>
      <max value="*"/>
      <type>
        <code value="Extension"/>
      </type>
      <mapping>
        <identity value="rim"/>
        <map value="n/a"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension.url"/>
      <representation value="xmlAttr"/>
      <short value="identifies the meaning of the extension"/>
      <definition value="Source of the definition for the extension code - a logical name or a URL."/>
      <comments value="The definition may point directly to a computable or human-readable definition of the extensibility codes, or it may be a logical URI as declared in some other specification. The definition should be version specific.  This will ideally be the URI for the Resource Profile defining the extension, with the code for the extension after a #."/>
      <min value="1"/>
      <max value="1"/>
      <type>
        <code value="uri"/>
      </type>
      <fixedUri value="type"/>
      <mapping>
        <identity value="rim"/>
        <map value="N/A"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension.valueCode"/>
      <short value="Value of extension"/>
      <definition value="Value of extension - may be a resource or one of a constrained set of the data types (see Extensibility in the spec for list)."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="code"/>
      </type>
      <binding>
        <strength value="required"/>
        <description value="The type of related resource for the module"/>
        <valueSetReference>
          <reference value="http://hl7.org/fhir/ValueSet/knowledge-module-resource-type"/>
        </valueSetReference>
      </binding>
      <mapping>
        <identity value="rim"/>
        <map value="N/A"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension"/>
      <name value="uri"/>
      <short value="Extension"/>
      <definition value="The uri of the related resource."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="Extension"/>
      </type>
    </element>
    <element>
      <path value="Extension.extension.id"/>
      <representation value="xmlAttr"/>
      <short value="xml:id (or equivalent in JSON)"/>
      <definition value="unique id for the element within a resource (for internal references)."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="id"/>
      </type>
      <mapping>
        <identity value="rim"/>
        <map value="n/a"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension.extension"/>
      <short value="Additional Content defined by implementations"/>
      <definition value="May be used to represent additional information that is not part of the basic definition of the element. In order to make the use of extensions safe and manageable, there is a strict set of governance  applied to the definition and use of extensions. Though any implementer is allowed to define an extension, there is a set of requirements that SHALL be met as part of the definition of the extension."/>
      <comments value="There can be no stigma associated with the use of extensions by any application, project, or standard - regardless of the institution or jurisdiction that uses or defines the extensions.  The use of extensions is what allows the FHIR specification to retain a core level of simplicity for everyone."/>
      <alias value="extensions"/>
      <alias value="user content"/>
      <min value="0"/>
      <max value="*"/>
      <type>
        <code value="Extension"/>
      </type>
      <mapping>
        <identity value="rim"/>
        <map value="n/a"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension.url"/>
      <representation value="xmlAttr"/>
      <short value="identifies the meaning of the extension"/>
      <definition value="Source of the definition for the extension code - a logical name or a URL."/>
      <comments value="The definition may point directly to a computable or human-readable definition of the extensibility codes, or it may be a logical URI as declared in some other specification. The definition should be version specific.  This will ideally be the URI for the Resource Profile defining the extension, with the code for the extension after a #."/>
      <min value="1"/>
      <max value="1"/>
      <type>
        <code value="uri"/>
      </type>
      <fixedUri value="uri"/>
      <mapping>
        <identity value="rim"/>
        <map value="N/A"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension.valueUri"/>
      <short value="Value of extension"/>
      <definition value="Value of extension - may be a resource or one of a constrained set of the data types (see Extensibility in the spec for list)."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="uri"/>
      </type>
      <mapping>
        <identity value="rim"/>
        <map value="N/A"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension"/>
      <name value="description"/>
      <short value="Extension"/>
      <definition value="A brief description of the related resource."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="Extension"/>
      </type>
    </element>
    <element>
      <path value="Extension.extension.id"/>
      <representation value="xmlAttr"/>
      <short value="xml:id (or equivalent in JSON)"/>
      <definition value="unique id for the element within a resource (for internal references)."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="id"/>
      </type>
      <mapping>
        <identity value="rim"/>
        <map value="n/a"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension.extension"/>
      <short value="Additional Content defined by implementations"/>
      <definition value="May be used to represent additional information that is not part of the basic definition of the element. In order to make the use of extensions safe and manageable, there is a strict set of governance  applied to the definition and use of extensions. Though any implementer is allowed to define an extension, there is a set of requirements that SHALL be met as part of the definition of the extension."/>
      <comments value="There can be no stigma associated with the use of extensions by any application, project, or standard - regardless of the institution or jurisdiction that uses or defines the extensions.  The use of extensions is what allows the FHIR specification to retain a core level of simplicity for everyone."/>
      <alias value="extensions"/>
      <alias value="user content"/>
      <min value="0"/>
      <max value="*"/>
      <type>
        <code value="Extension"/>
      </type>
      <mapping>
        <identity value="rim"/>
        <map value="n/a"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension.url"/>
      <representation value="xmlAttr"/>
      <short value="identifies the meaning of the extension"/>
      <definition value="Source of the definition for the extension code - a logical name or a URL."/>
      <comments value="The definition may point directly to a computable or human-readable definition of the extensibility codes, or it may be a logical URI as declared in some other specification. The definition should be version specific.  This will ideally be the URI for the Resource Profile defining the extension, with the code for the extension after a #."/>
      <min value="1"/>
      <max value="1"/>
      <type>
        <code value="uri"/>
      </type>
      <fixedUri value="description"/>
      <mapping>
        <identity value="rim"/>
        <map value="N/A"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension.valueString"/>
      <short value="Value of extension"/>
      <definition value="Value of extension - may be a resource or one of a constrained set of the data types (see Extensibility in the spec for list)."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="string"/>
      </type>
      <mapping>
        <identity value="rim"/>
        <map value="N/A"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension"/>
      <name value="document"/>
      <short value="Extension"/>
      <definition value="The document being referenced."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="Extension"/>
      </type>
    </element>
    <element>
      <path value="Extension.extension.id"/>
      <representation value="xmlAttr"/>
      <short value="xml:id (or equivalent in JSON)"/>
      <definition value="unique id for the element within a resource (for internal references)."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="id"/>
      </type>
      <mapping>
        <identity value="rim"/>
        <map value="n/a"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension.extension"/>
      <short value="Additional Content defined by implementations"/>
      <definition value="May be used to represent additional information that is not part of the basic definition of the element. In order to make the use of extensions safe and manageable, there is a strict set of governance  applied to the definition and use of extensions. Though any implementer is allowed to define an extension, there is a set of requirements that SHALL be met as part of the definition of the extension."/>
      <comments value="There can be no stigma associated with the use of extensions by any application, project, or standard - regardless of the institution or jurisdiction that uses or defines the extensions.  The use of extensions is what allows the FHIR specification to retain a core level of simplicity for everyone."/>
      <alias value="extensions"/>
      <alias value="user content"/>
      <min value="0"/>
      <max value="*"/>
      <type>
        <code value="Extension"/>
      </type>
      <mapping>
        <identity value="rim"/>
        <map value="n/a"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension.url"/>
      <representation value="xmlAttr"/>
      <short value="identifies the meaning of the extension"/>
      <definition value="Source of the definition for the extension code - a logical name or a URL."/>
      <comments value="The definition may point directly to a computable or human-readable definition of the extensibility codes, or it may be a logical URI as declared in some other specification. The definition should be version specific.  This will ideally be the URI for the Resource Profile defining the extension, with the code for the extension after a #."/>
      <min value="1"/>
      <max value="1"/>
      <type>
        <code value="uri"/>
      </type>
      <fixedUri value="document"/>
      <mapping>
        <identity value="rim"/>
        <map value="N/A"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.extension.valueReference"/>
      <short value="Value of extension"/>
      <definition value="Value of extension - may be a resource or one of a constrained set of the data types (see Extensibility in the spec for list)."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="Reference"/>
        <profile value="http://hl7.org/fhir/StructureDefinition/DocumentReference"/>
      </type>
      <mapping>
        <identity value="rim"/>
        <map value="N/A"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.url"/>
      <representation value="xmlAttr"/>
      <short value="identifies the meaning of the extension"/>
      <definition value="Source of the definition for the extension code - a logical name or a URL."/>
      <comments value="The definition may point directly to a computable or human-readable definition of the extensibility codes, or it may be a logical URI as declared in some other specification. The definition should be version specific.  This will ideally be the URI for the Resource Profile defining the extension, with the code for the extension after a #."/>
      <min value="1"/>
      <max value="1"/>
      <type>
        <code value="uri"/>
      </type>
      <fixedUri value="http://hl7.org/fhir/StructureDefinition/basic-cqif-relatedResource"/>
      <mapping>
        <identity value="rim"/>
        <map value="N/A"/>
      </mapping>
    </element>
    <element>
      <path value="Extension.value[x]"/>
      <short value="Value of extension"/>
      <definition value="Value of extension - may be a resource or one of a constrained set of the data types (see Extensibility in the spec for list)."/>
      <min value="0"/>
      <max value="0"/>
      <type>
        <code value="*"/>
      </type>
      <mapping>
        <identity value="rim"/>
        <map value="N/A"/>
      </mapping>
    </element>
  </snapshot>
  <differential>
    <element>
      <path value="Extension"/>
      <definition value="Related resources such as additional documentation, supporting evidence, or bibliographic references."/>
      <min value="0"/>
      <max value="*"/>
      <type>
        <code value="Extension"/>
      </type>
    </element>
    <element>
      <path value="Extension.extension"/>
      <name value="type"/>
      <definition value="The type of related resource."/>
      <min value="1"/>
      <max value="1"/>
      <type>
        <code value="Extension"/>
      </type>
    </element>
    <element>
      <path value="Extension.extension.url"/>
      <type>
        <code value="uri"/>
      </type>
      <fixedUri value="type"/>
    </element>
    <element>
      <path value="Extension.extension.value[x]"/>
      <type>
        <code value="code"/>
      </type>
      <binding>
        <strength value="required"/>
        <description value="The type of related resource for the module"/>
        <valueSetReference>
          <reference value="http://hl7.org/fhir/ValueSet/knowledge-module-resource-type"/>
        </valueSetReference>
      </binding>
    </element>
    <element>
      <path value="Extension.extension"/>
      <name value="uri"/>
      <definition value="The uri of the related resource."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="Extension"/>
      </type>
    </element>
    <element>
      <path value="Extension.extension.url"/>
      <type>
        <code value="uri"/>
      </type>
      <fixedUri value="uri"/>
    </element>
    <element>
      <path value="Extension.extension.value[x]"/>
      <type>
        <code value="uri"/>
      </type>
    </element>
    <element>
      <path value="Extension.extension"/>
      <name value="description"/>
      <definition value="A brief description of the related resource."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="Extension"/>
      </type>
    </element>
    <element>
      <path value="Extension.extension.url"/>
      <type>
        <code value="uri"/>
      </type>
      <fixedUri value="description"/>
    </element>
    <element>
      <path value="Extension.extension.value[x]"/>
      <type>
        <code value="string"/>
      </type>
    </element>
    <element>
      <path value="Extension.extension"/>
      <name value="document"/>
      <definition value="The document being referenced."/>
      <min value="0"/>
      <max value="1"/>
      <type>
        <code value="Extension"/>
      </type>
    </element>
    <element>
      <path value="Extension.extension.url"/>
      <type>
        <code value="uri"/>
      </type>
      <fixedUri value="document"/>
    </element>
    <element>
      <path value="Extension.extension.value[x]"/>
      <type>
        <code value="Reference"/>
        <profile value="http://hl7.org/fhir/StructureDefinition/DocumentReference"/>
      </type>
    </element>
    <element>
      <path value="Extension.url"/>
      <type>
        <code value="uri"/>
      </type>
      <fixedUri value="http://hl7.org/fhir/StructureDefinition/basic-cqif-relatedResource"/>
    </element>
    <element>
      <path value="Extension.value[x]"/>
      <min value="0"/>
      <max value="0"/>
    </element>
  </differential>
</StructureDefinition>